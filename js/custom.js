


/* Category Menu
============================================================*/

// Button Click +++
$(document).on('click', '.hdr_bd_cat_btn', function (e) {
	e.preventDefault();
	
	$(this).toggleClass('active');
	$('.cat_menu_bx').toggleClass('active');
});

// Close
$(document).on('click', function(e) {
	if ($(e.target).is('.hdr_bd_cat_btn, .hdr_bd_cat_btn *, .cat_menu, .cat_menu') === false) {
		$('.hdr_bd_cat_btn').removeClass('active');
		$('.cat_menu_bx').removeClass('active');
	}
});
// Button Click ---



// Menu +++
$(document).on('mouseenter', '.cat_menu_ls > li a', function (e) {
	e.stopPropagation();

	$(this).parent().addClass('active').siblings().removeClass('active');
	
	$('.cat_drop_it').eq($(this).parent().index()).addClass('active').siblings().removeClass('active');
});
// Menu ---

/*========== Category Menu ==========*/






/* Header Foot Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.hdr_ft_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			// transitionStyle : 'fade',
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .hdr_ft_bnr');
	}
});
/*========== Header Foot Banner ==========*/






/* Reklama Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.rek_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			// transitionStyle : 'fade',
		});



		// Custom Navigation Events
		$('.rek_bnr_btn.prev').click(function () {
			$(this).parent().siblings('.rek_bnr').trigger('owl.prev');
		});

		$('.rek_bnr_btn.next').click(function () {
			$(this).parent().siblings('.rek_bnr').trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .rek_bnr');
	}
});
/*========== Reklama Banner ==========*/






/* Star Rate
============================================================*/
$(document).on('click', '.product_v_like', function (e) {
	e.preventDefault();

	$(this).toggleClass('active');

});
/*========== Star Rate ==========*/






/* Client Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.client_idx_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				[768, 3],
				[992, 4],
				[1200, 5],
			],
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			// transitionStyle : 'fade',
		});



		// Custom Navigation Events
		$('.client_idx_bnr_btn.prev').click(function () {
			$(this).parent().siblings('.client_idx_bnr').trigger('owl.prev');
		});

		$('.client_idx_bnr_btn.next').click(function () {
			$(this).parent().siblings('.client_idx_bnr').trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .client_idx_bnr');
	}
});
/*========== Client Banner ==========*/








/* Star Rate
============================================================*/
$(document).on('click', '.base_star_bx.rate .base_star i', function () {

	$(this).addClass('active');
	$(this).prevAll().addClass('active');
	$(this).nextAll().removeClass('active');

});
/*========== Star Rate ==========*/










/* International Tel Input
============================================================*/
try {
	$('.int_tel').intlTelInput({
		// allowDropdown: false,
		// autoHideDialCode: false,
		// autoPlaceholder: "off",
		// dropdownContainer: "body",
		// excludeCountries: ["us"],
		// formatOnDisplay: false,
		geoIpLookup: function (callback) {
			$.get("http://ipinfo.io", function () {
			}, "jsonp").always(function (resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		// initialCountry: "auto",
		// nationalMode: false,
		onlyCountries: ['uz', 'ru', 'en', 'kz', 'kg', 'kr'],
		// placeholderNumberType: "MOBILE",
		preferredCountries: ['uz'],
		// separateDialCode: true,
		// utilsScript: "lib/int_tel_input/js/utils.js"
	});
}
catch (e) {
	console.warn("International Tel Input cannot find elements");
}
/*========== International Tel Input ==========*/











/* Goods File Upload
============================================================*/
$(document).on('change', '.pc_good_1_upl_lbl input', function () {
	var fileName = $(this).val().split("\\").pop();

	if (fileName) {
		$('.pc_good_1_upl_inp').text(fileName);
	}
	else {
		$('.pc_good_1_upl_inp').text('Файл не выбран...');
	}
});
/*========== Goods File Upload ==========*/











/* Delivery Address Add
============================================================*/
$(document).on('click', '.dlvr_adrs_saved_btn', function (e) {
	e.preventDefault();
	
	$(this).addClass('active');
	$('.dlvr_adrs_new').slideDown();
});
/*========== Delivery Address Add ==========*/











/* Image Upload
============================================================*/
$('.img_upload > .inp').on('change', function () {
	var elem = $(this);
	

	if (this.files && this.files[0]) {
		var reader = new FileReader();

		reader.onload = function () {

			elem.parent().addClass('active');
			elem.siblings('.img').attr('src', reader.result);

		};

		reader.readAsDataURL(this.files[0]);
	}
	else {
		console.log('Not uploaded');
	}
});
/*========== Image Upload ==========*/









/* Modal Authorization Tab
============================================================*/
$(document).on('click', '.hdr_hd_reg a', function (e) {
	$($(this).attr('href')).tab('show');
});
/*========== Modal Authorization Tab ==========*/





/* Modal Authorization Telephone
============================================================*/
$(document).on('click', '[data-code]', function (e) {
	e.preventDefault();

	$('[data-code-active]').attr('disabled', true);

	$('[data-code-disabled]').attr('disabled', false).focus();
});
/*========== Modal Authorization Telephone ==========*/










/* Product Slider
============================================================*/
try {
	$('.prod_one_view_img_lg').slick({
		asNavFor: '.prod_one_view_img_sm',
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: true,
		infinite: false,
		arrows: false,
		fade: true
	});


	$('.prod_one_view_img_sm').slick({
		asNavFor: '.prod_one_view_img_lg',
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		focusOnSelect: true,
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	});
}
catch (e) {
	console.warn('Slick cannot find .prod_one_view_img_lg, .prod_one_view_img_sm');
}
/*========== Product Slider ==========*/













/* Product Anchor
============================================================*/
$(document).on('click', '.prod_one_inst_anchor', function (e) {
	e.preventDefault();
	
	$('html, body').animate({ scrollTop: 0 }, 700);
});
/*========== Product Anchor ==========*/













/* Filter Group
============================================================*/
$(document).on('click', '.filter_group_ttl', function () {
	$(this).toggleClass('active').siblings('.filter_group_cnt').slideToggle();
});
/*========== Filter Group ==========*/












/* Price Range Slider
============================================================*/
$(document).ready(function () {
	try {
		var range_slider = $('[data-price-range]');

		
		// Initialize
		noUiSlider.create(range_slider[0], {
			start: [500, 2200000],
			range: {
				'min': [0, 1000],
				'50%': [100000, 50000],
				'70%': [1000000, 1000000],
				'90%': [10000000, 2000000],
				'max': [20000000]
			},
			// step: 100,
			connect: true,
			format: wNumb({
				decimals: 0,
				thousand: ' ',
				// suffix: ' UZS'
			})
		});




		// Update values of Inputs when Range updates
		range_slider[0].noUiSlider.on('update', function (values, handle) {

			if (handle) {

				// 2 - Button
				range_slider.siblings('.prod_all_ftr_prc_val_bx').find('[data-price-range-to]').val(values[handle]);
			} else {

				// 1 - Button
				range_slider.siblings('.prod_all_ftr_prc_val_bx').find('[data-price-range-from]').val(values[handle]);
			}
		});




		// Update Range when values of Inputs change
		$('[data-price-range-from]').on('change', function () {
			range_slider[0].noUiSlider.set(
				[$(this).val(), null]
			);
		});
		$('[data-price-range-to]').on('change', function () {
			range_slider[0].noUiSlider.set(
				[null, $(this).val()]
			);
		});
	}
	catch (e) {
		console.warn('noUiSlider cannot find elements');
	}
});
/*========== Price Range Slider ==========*/






















/* Header
============================================================*/

/*========== Header ==========*/


